import { Injectable } from '@nestjs/common';
import { S_IFMT } from 'constants';
import { LoginDto } from './entities/login.dto';
import { UserDto } from './entities/user.dto';
const admin = require('firebase-admin');
import * as jwt from 'jsonwebtoken';
@Injectable()
export class AppService {

  constructor() {
    this.getAccessToken();
  }

  async getAccessToken() {
    try {
      var filepath = process.env.FIRESTORE_APPLICATION_CREDENTIALS;
      let serviceAccount = require(filepath);
      console.log('serviceAccount', serviceAccount);
      admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
      });
    } catch (error) {
      console.error(error);
    }
  }
  async login(user: LoginDto) {
    try {
      const result = []
      let db = admin.firestore();
      let docRef = db.collection('users');
      const response = await docRef.where('email', '==', user.email).where('password', '==', user.password).get();
      if (response.size > 0) {
        response.forEach((doc) => {
          result.push(doc.data());
        });
      }
      if (result.length > 0) {
        const secret = process.env.JWT_SECRET;
        const token = jwt.sign(
          {
            nombre: result[0].nombre,
            email: result[0].email,
            rut: result[0].rut,
          },
          secret,
          {
            expiresIn: 60000,
          }
        );
        return {token};
      } else {
        return null;
      }


    } catch (error) {
      console.error(error);
    }
  }
  register(user: UserDto): boolean {
    const a = JSON.parse(JSON.stringify(user))
    const db = admin.firestore();
    let docRef = db.collection('users');
    return docRef.doc().set(a)
      .then(info => {
        console.log(info);
        return true;
      })
      .catch(error => {
        console.error(error);
        return false;
      });
  }

  async obtenerUsuarios() {
    try {
      const result = []
      let db = admin.firestore();
      let docRef = db.collection('users');
      const response = await docRef.get();
      if (response.size > 0) {
        response.forEach((doc) => {
          result.push(doc.data());
        });
      }
      return result;
    } catch (error) {
      console.error(error);

    }

  }

}
