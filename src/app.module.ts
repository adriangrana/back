import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
require('dotenv').config();

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;
  static title: string;
  static description: string;
  static version: string;

  constructor(
  ) {
      this.init();
  }
  async init() {
    AppModule.port = process.env.NODE_PORT;
    AppModule.title ='desafio';
    AppModule.description = 'desafio sura';
    AppModule.version = '1.0.0';
  }
 
}
