import { Body, Controller, Get, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { AppService } from './app.service';
import { UserDto } from './entities/user.dto';
import { LoginDto } from './entities/login.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }
  @UsePipes(new ValidationPipe({ transform: true }))
  @Post('register')
  @ApiBearerAuth('access-token')
  register(@Body() user: UserDto): boolean {
   return this.appService.register(user);
  }
  @UsePipes(new ValidationPipe({ transform: true }))
  @Post('login')
  @ApiBearerAuth('access-token')
  login(@Body() login: LoginDto): any {
    return this.appService.login(login);
  }
  @UsePipes(new ValidationPipe({ transform: true }))
  @Get('obtenerUsuarios')
  @ApiBearerAuth('access-token')
  obtenerUsuarios(): any {
    return this.appService.obtenerUsuarios();
  }
}
